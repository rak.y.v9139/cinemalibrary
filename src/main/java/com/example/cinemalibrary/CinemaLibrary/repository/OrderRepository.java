package com.example.cinemalibrary.CinemaLibrary.repository;

import com.example.cinemalibrary.CinemaLibrary.model.Order;

import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends GenericRepository<Order>{
}
