package com.example.cinemalibrary.CinemaLibrary.repository;

import com.example.cinemalibrary.CinemaLibrary.model.Film;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository extends GenericRepository<Film>{
}
