package com.example.cinemalibrary.CinemaLibrary.repository;

import com.example.cinemalibrary.CinemaLibrary.model.Director;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Director>{
}
