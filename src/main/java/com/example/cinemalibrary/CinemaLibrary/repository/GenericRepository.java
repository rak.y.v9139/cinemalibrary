package com.example.cinemalibrary.CinemaLibrary.repository;

import com.example.cinemalibrary.CinemaLibrary.model.GenericModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface GenericRepository <T extends GenericModel> extends JpaRepository<T, Long> {
}
