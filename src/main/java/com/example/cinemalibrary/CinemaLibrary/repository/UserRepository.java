package com.example.cinemalibrary.CinemaLibrary.repository;

import com.example.cinemalibrary.CinemaLibrary.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository
        extends GenericRepository<User> {

    //select * from users where login = ?
//    @Query(nativeQuery = true, value = "select * from users where login = :login")
    User findUserByLogin(String login);

    User findUserByEmail(String email);

}
