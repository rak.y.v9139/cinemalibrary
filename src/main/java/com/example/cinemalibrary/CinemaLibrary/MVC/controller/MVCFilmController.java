package com.example.cinemalibrary.CinemaLibrary.MVC.controller;

import com.example.cinemalibrary.CinemaLibrary.dto.FilmDTO;
import com.example.cinemalibrary.CinemaLibrary.dto.FilmWithDirectorsDTO;
import com.example.cinemalibrary.CinemaLibrary.service.DirectorService;
import com.example.cinemalibrary.CinemaLibrary.service.FilmService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/films")
public class MVCFilmController {
    private final DirectorService directorService;
    private final FilmService filmService;

    public MVCFilmController(FilmService filmService, DirectorService directorService) {
        this.filmService = filmService;
        this.directorService = directorService;
    }

    @GetMapping("")
    public String getAll(Model model) {
        List<FilmWithDirectorsDTO> result = filmService.getAllFilmsWithDirectors();
        model.addAttribute("films", result);
        return "films/viewAllFilms";
    }

    @GetMapping("/add")
    public String create() {

        return "films/addFilm";
    }


    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDTO filmDTO) {
        filmService.create(filmDTO);
        return "redirect:/films";
    }


}

