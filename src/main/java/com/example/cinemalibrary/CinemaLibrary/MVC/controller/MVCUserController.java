package com.example.cinemalibrary.CinemaLibrary.MVC.controller;

import com.example.cinemalibrary.CinemaLibrary.dto.UserDTO;
import com.example.cinemalibrary.CinemaLibrary.service.UserService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.ToString;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.example.cinemalibrary.CinemaLibrary.constants.UserRolesConstants.ADMIN;
@ToString
@Controller
@Hidden
@RequestMapping("/users")
public class MVCUserController {
    private final UserService userService;

    public MVCUserController(UserService userService) {
        this.userService = userService;
    }
    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new UserDTO());
        return "registration";
    }


    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm" ) UserDTO userDTO, Model model,
                               BindingResult bindingResult){
        if (userDTO.getLogin().equalsIgnoreCase(ADMIN) || userService.getUserByLogin(userDTO.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login", "Такой логин уже существует");
            return "registration";
        }
        if (userService.getUserByEMail(userDTO.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.email", "Такой email уже существует");
            return "registration";
        }

        userService.create(userDTO);
        return "redirect:login";
    }
}
