package com.example.cinemalibrary.CinemaLibrary.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "films")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "films_gen", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class Film extends GenericModel {

    @Column(name = "title")
    private String title;

    @Column(name = "premier_year")
    private LocalDate premierYear;

    @Column(name = "country")
    private String country;

    @Column(name = "genre")
    @Enumerated
    private Genre genre;
@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},
        fetch = FetchType.LAZY)
@JoinTable(name = "films_directors",
joinColumns = @JoinColumn(name = "film_id"),foreignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"),
inverseJoinColumns = @JoinColumn(name = "director_id"),inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS"))
private Set<Director> directors;
@OneToMany(mappedBy = "film")
private Set<Order> orders;

}
