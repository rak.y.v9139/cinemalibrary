package com.example.cinemalibrary.CinemaLibrary.dto;

import com.example.cinemalibrary.CinemaLibrary.model.Genre;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor

public class FilmDTO extends GenericDTO {

    private String title;
    private String premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorsIds;
}
