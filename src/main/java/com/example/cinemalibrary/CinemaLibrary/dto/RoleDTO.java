package com.example.cinemalibrary.CinemaLibrary.dto;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RoleDTO extends GenericDTO{

    private Long id;
    private String title;
    private String description;
}

