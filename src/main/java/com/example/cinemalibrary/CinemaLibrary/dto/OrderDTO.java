package com.example.cinemalibrary.CinemaLibrary.dto;

import com.example.cinemalibrary.CinemaLibrary.model.Film;
import com.example.cinemalibrary.CinemaLibrary.model.User;
import jakarta.persistence.Column;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderDTO extends GenericDTO {
    private Long userId;
    private Long filmId;
    private LocalDateTime rentDate;
    private Integer rentPeriod;


}
