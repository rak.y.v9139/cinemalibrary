package com.example.cinemalibrary.CinemaLibrary.controller;

import com.example.cinemalibrary.CinemaLibrary.dto.UserDTO;
import com.example.cinemalibrary.CinemaLibrary.model.User;
import com.example.cinemalibrary.CinemaLibrary.repository.GenericRepository;
import com.example.cinemalibrary.CinemaLibrary.repository.UserRepository;
import com.example.cinemalibrary.CinemaLibrary.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
@Tag(name = "пользователи",
        description = "Контроллер для работы с пользователями")
public class UserController extends GenericController<User, UserDTO> {

    public UserController(UserService userService) {
        super(userService);
    }
}
