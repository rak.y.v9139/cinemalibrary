package com.example.cinemalibrary.CinemaLibrary.controller;

import com.example.cinemalibrary.CinemaLibrary.dto.OrderDTO;
import com.example.cinemalibrary.CinemaLibrary.model.Order;
import com.example.cinemalibrary.CinemaLibrary.service.OrderService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/order")
@Tag(name = "Заказы", description = "Контроллер для работы с заказами")
public class OrderController extends GenericController<Order, OrderDTO> {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        super(orderService);
        this.orderService = orderService;

    }

//    @Operation(description = "Аренда фильма", method = "rentFilm")
//    @RequestMapping(value = "/rentFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//public ResponseEntity<OrderDTO> rentFilm(@RequestParam(value = "filmId") Long filmId,
//                                         @RequestParam(value = "userId") Long userId,
//                                         @RequestParam(value = "period") Integer period){
//        OrderDTO orderDTO = orderService.rentFilm(filmId,userId,period);
//        return ResponseEntity.status(HttpStatus.CREATED).body(orderService.update(orderDTO));
//    }



}