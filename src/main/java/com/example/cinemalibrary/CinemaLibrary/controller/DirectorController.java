package com.example.cinemalibrary.CinemaLibrary.controller;

import com.example.cinemalibrary.CinemaLibrary.dto.DirectorDTO;
import com.example.cinemalibrary.CinemaLibrary.model.Director;
import com.example.cinemalibrary.CinemaLibrary.service.DirectorService;
import com.example.cinemalibrary.CinemaLibrary.service.FilmService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/director")
@Tag(name = "Режиссеры", description = "Контроллер для работы с режиссерами")
public class DirectorController extends GenericController<Director, DirectorDTO> {


    private final DirectorService directorService;


    public DirectorController(FilmService filmService, DirectorService directorService) {
        super(directorService);
        this.directorService = directorService;

    }
//
//    @Operation(description = "Добавить фильм к режиссеру", method = "addFilm")
//    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "directorId") Long directorId, @RequestParam(value = "filmId") Long filmId) {
//        DirectorDTO director = directorService.addFilmDirector(directorId,filmId);
//        return ResponseEntity.status(HttpStatus.OK).body(directorService.update(director));
//    }
}
