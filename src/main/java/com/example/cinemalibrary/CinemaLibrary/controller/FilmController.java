package com.example.cinemalibrary.CinemaLibrary.controller;

import com.example.cinemalibrary.CinemaLibrary.dto.FilmDTO;
import com.example.cinemalibrary.CinemaLibrary.model.Film;
import com.example.cinemalibrary.CinemaLibrary.service.DirectorService;
import com.example.cinemalibrary.CinemaLibrary.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/film")
@Tag(name = "Фильмы",
        description = "Контроллер для работы с фильмами фильмотеки"
)
public class FilmController extends GenericController<Film, FilmDTO> {
    private final FilmService filmService;


    public FilmController(FilmService filmService, DirectorService directorService) {
        super(filmService);
        this.filmService = filmService;
    }

//    @Operation(description = "Добавить режиссера к фильму", method = "addDirector")
//    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<FilmDTO> addDirector(@RequestParam(value = "filmId") Long filmId,
//                                            @RequestParam(value = "directorId") Long directorId){
//        FilmDTO filmDTO = filmService.addDirectorFilm(filmId , directorId);
//
//       return ResponseEntity.status(HttpStatus.OK).body(filmService.update(filmDTO));
//    }
}
