package com.example.cinemalibrary.CinemaLibrary.service;

import com.example.cinemalibrary.CinemaLibrary.dto.FilmDTO;
import com.example.cinemalibrary.CinemaLibrary.dto.OrderDTO;
import com.example.cinemalibrary.CinemaLibrary.dto.UserDTO;
import com.example.cinemalibrary.CinemaLibrary.mapper.OrderMapper;
import com.example.cinemalibrary.CinemaLibrary.model.Film;
import com.example.cinemalibrary.CinemaLibrary.model.Order;
import com.example.cinemalibrary.CinemaLibrary.model.User;
import com.example.cinemalibrary.CinemaLibrary.repository.FilmRepository;
import com.example.cinemalibrary.CinemaLibrary.repository.OrderRepository;
import com.example.cinemalibrary.CinemaLibrary.repository.UserRepository;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final FilmRepository filmRepository;
    private final OrderMapper orderMapper;


    protected OrderService( OrderRepository orderRepository, OrderMapper orderMapper, UserRepository userRepository, FilmRepository filmRepository) {
        super(orderRepository, orderMapper);
        this.userRepository = userRepository;
        this.orderRepository = orderRepository;
        this.filmRepository = filmRepository;
        this.orderMapper = orderMapper;


    }

    public OrderDTO  rentFilm(Long filmId , Long userId, Integer period) {
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("Пользователя нет"));
        OrderDTO orderDTO = new OrderDTO(userId,filmId,LocalDateTime.now(),period);
        orderDTO.setCreatedBy("thisUser");
        orderDTO.setCreatedWhen(new Date());
        Order order = orderMapper.toEntity(orderDTO);
        user.getOrders().add(order);
        return orderMapper.toDTO(order);


//        Order order = new Order();
//        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильма нет"));
//        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("Пользователя нет"));
//        order.setFilm(film);
//        order.setUser(user);
//        order.setCreatedWhen(LocalDate.now());
//        user.getOrders().add(order);
//        return orderMapper.toDTO(order);


    }


}
