package com.example.cinemalibrary.CinemaLibrary.service;

import com.example.cinemalibrary.CinemaLibrary.dto.DirectorDTO;
import com.example.cinemalibrary.CinemaLibrary.dto.DirectorWithFilmsDTO;
import com.example.cinemalibrary.CinemaLibrary.dto.FilmDTO;
import com.example.cinemalibrary.CinemaLibrary.mapper.DirectorMapper;
import com.example.cinemalibrary.CinemaLibrary.mapper.DirectorWithFilmsMapper;
import com.example.cinemalibrary.CinemaLibrary.model.Director;
import com.example.cinemalibrary.CinemaLibrary.model.Film;
import com.example.cinemalibrary.CinemaLibrary.repository.DirectorRepository;
import com.example.cinemalibrary.CinemaLibrary.repository.FilmRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class DirectorService extends GenericService<Director, DirectorDTO> {
    private final FilmRepository filmRepository;
    private final DirectorRepository directorRepository;
    private final DirectorWithFilmsMapper directorWithFilmsMapper;

    private final DirectorMapper directorMapper;
    protected DirectorService(DirectorRepository repository, DirectorMapper directorMapper, FilmRepository filmRepository , DirectorWithFilmsMapper directorWithFilmsMapper) {
        super(repository, directorMapper);
        this.directorMapper = directorMapper;
        this.directorRepository = repository;
        this.filmRepository = filmRepository;
        this.directorWithFilmsMapper = directorWithFilmsMapper;
    }

    public DirectorDTO addFilmDirector(Long idDirector, Long idFilm){
        Director director = directorRepository.findById(idDirector).orElseThrow(() -> new NotFoundException("Пользователя нет"));
        Film film = filmRepository.findById(idFilm).orElseThrow(() -> new NotFoundException("Фильма нет"));
        director.getFilms().add(film);
        return directorMapper.toDTO(director);
    }

    public List<DirectorWithFilmsDTO> getAllDirectorWithFilms(){
        return directorWithFilmsMapper.toDTOs(directorRepository.findAll());
    }
}
