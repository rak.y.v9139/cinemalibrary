package com.example.cinemalibrary.CinemaLibrary.service;

import com.example.cinemalibrary.CinemaLibrary.dto.RoleDTO;
import com.example.cinemalibrary.CinemaLibrary.dto.UserDTO;
import com.example.cinemalibrary.CinemaLibrary.mapper.UserMapper;
import com.example.cinemalibrary.CinemaLibrary.model.User;
import com.example.cinemalibrary.CinemaLibrary.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

@Service
public class UserService extends GenericService<User, UserDTO>{

    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    protected UserService(UserRepository userRepository,
                          UserMapper userMapper,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        super(userRepository,userMapper);
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public UserDTO getUserByLogin(final String login){
        return mapper.toDTO(((UserRepository) repository).findUserByLogin(login));
    }
    public UserDTO getUserByEMail(final String eMail){
        return mapper.toDTO(((UserRepository) repository).findUserByEmail(eMail));
    }

    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        object.setRole(roleDTO);
        object.setCreatedBy("REGISTRATION FORM");
        object.setCreatedWhen(new Date());
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        System.out.println(object.toString());
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }
}


