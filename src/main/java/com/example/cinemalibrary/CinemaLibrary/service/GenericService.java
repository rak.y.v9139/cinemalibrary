package com.example.cinemalibrary.CinemaLibrary.service;

import com.example.cinemalibrary.CinemaLibrary.dto.GenericDTO;
import com.example.cinemalibrary.CinemaLibrary.mapper.GenericMapper;
import com.example.cinemalibrary.CinemaLibrary.model.GenericModel;
import com.example.cinemalibrary.CinemaLibrary.repository.GenericRepository;
import jakarta.persistence.PrePersist;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Абстрактный сервис, который хранит в себе реализацию CRUD операций по-умолчанию.
 * Если реализация отличная от того, что представлено в этом классе,
 * то она переопределяется в реализации конкретного сервиса.
 *
 * @param <T> - Сущность, с которой мы работаем.
 * @param <N> - DTO, которую мы будем отдавать/принимать дальше.
 */
@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {
    protected final GenericRepository<T> repository;

    protected final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository, GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> listAll() {
        return mapper.toDTOs(repository.findAll());
    }

    public N getOne(final Long id) {
        return mapper.toDTO(repository.findById(id).orElseThrow(() -> new NotFoundException("Данных по заданному id нет.")));
    }

    public N create(N newObject) {
        newObject.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        newObject.setCreatedWhen(new Date());
        System.out.println(newObject.getCreatedWhen());
        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    public N update(N updateObject){
        return mapper.toDTO(repository.save(mapper.toEntity(updateObject)));
    }

    public void delete(Long id){
        repository.deleteById(id);
    }


}
