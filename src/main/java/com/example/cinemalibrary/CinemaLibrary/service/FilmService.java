package com.example.cinemalibrary.CinemaLibrary.service;

import com.example.cinemalibrary.CinemaLibrary.dto.FilmDTO;
import com.example.cinemalibrary.CinemaLibrary.dto.FilmWithDirectorsDTO;
import com.example.cinemalibrary.CinemaLibrary.mapper.FilmMapper;
import com.example.cinemalibrary.CinemaLibrary.mapper.FilmWithDirectorsMapper;
import com.example.cinemalibrary.CinemaLibrary.model.Film;
import com.example.cinemalibrary.CinemaLibrary.repository.FilmRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmService extends GenericService<Film, FilmDTO> {
    private final FilmRepository filmRepository;
private final FilmWithDirectorsMapper filmWithDirectorsMapper;
    protected FilmService(FilmRepository filmRepository,   FilmMapper filmMapper, FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(filmRepository, filmMapper);
        this.filmRepository = filmRepository;

        this.filmWithDirectorsMapper = filmWithDirectorsMapper;


    }

    public List<FilmWithDirectorsDTO> getAllFilmsWithDirectors() {
        return filmWithDirectorsMapper.toDTOs(filmRepository.findAll());
    }

}
