package com.example.cinemalibrary.CinemaLibrary.mapper;

import com.example.cinemalibrary.CinemaLibrary.dto.OrderDTO;
import com.example.cinemalibrary.CinemaLibrary.model.Order;
import com.example.cinemalibrary.CinemaLibrary.repository.FilmRepository;
import com.example.cinemalibrary.CinemaLibrary.repository.UserRepository;
import com.example.cinemalibrary.CinemaLibrary.service.FilmService;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Set;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDTO> {
    private final FilmRepository filmRepository;
    private final UserRepository userRepository;



    protected OrderMapper(ModelMapper modelMapper,
                          UserRepository userRepository,
                          FilmRepository filmRepository) {
        super(modelMapper, Order.class, OrderDTO.class);

        this.filmRepository = filmRepository;
        this.userRepository = userRepository;


    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setUserId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(OrderDTO::setFilmId)).setPostConverter(toDTOConverter());
//                .addMappings(m -> m.skip(OrderDTO::setFilmDTO)).setPostConverter(toDTOConverter());


        super.modelMapper.createTypeMap(OrderDTO.class, Order.class)
                .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Order::setFilm)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow(() -> new NotFoundException("Фильм не найден")));
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователя не найдено")));

    }

    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setUserId(source.getUser().getId());
        destination.setFilmId(source.getFilm().getId());
//        destination.setFilmDTO(filmService.getOne(source.getFilm().getId()));

    }

    @Override
    protected Set<Long> getIds(Order entity) {
        return null;
    }
}
