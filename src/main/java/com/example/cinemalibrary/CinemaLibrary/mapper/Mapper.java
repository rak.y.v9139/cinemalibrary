package com.example.cinemalibrary.CinemaLibrary.mapper;

import com.example.cinemalibrary.CinemaLibrary.dto.GenericDTO;
import com.example.cinemalibrary.CinemaLibrary.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {

    E toEntity(D dto);

    D toDTO(E entity);

    List<E> toEntities(List<D> dtos);

    List<D> toDTOs(List<E> entities);
}
