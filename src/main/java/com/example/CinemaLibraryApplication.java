package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CinemaLibraryApplication {

    public static void main(String[] args) {
        SpringApplication.run(CinemaLibraryApplication.class, args);
    }

}
