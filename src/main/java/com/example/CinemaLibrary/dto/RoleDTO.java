package com.example.CinemaLibrary.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleDTO extends GenericDTO {
    private String roleTitle;
    private String roleDescription;
    private Set<Long> usersIds;
}



