package com.example.CinemaLibrary.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IdFilmDirectorDTO {
    private Long filmId;
    private Long directorId;
}
