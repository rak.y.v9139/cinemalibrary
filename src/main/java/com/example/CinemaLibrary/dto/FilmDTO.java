package com.example.CinemaLibrary.dto;


import com.example.CinemaLibrary.model.Director;
import com.example.CinemaLibrary.model.Film;
import com.example.CinemaLibrary.model.Genre;
import lombok.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilmDTO extends GenericDTO {
    private String filmTitle;
    private String premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorsIds;
    private Set<Long> ordersIds;
    private boolean isDeleted;

    public FilmDTO(Film film) {
        FilmDTO filmDTO = new FilmDTO();
        filmDTO.setFilmTitle(film.getFilmTitle());
        filmDTO.setPremierYear(film.getPremierYear().toString());
        filmDTO.setCountry(film.getCountry());
        filmDTO.setGenre(film.getGenre());
        Set<Director> directors = film.getDirectors();
        Set<Long> directorIds = new HashSet<>();
        if (directors != null && directors.size() > 0) {
            directors.forEach(a -> directorIds.add(a.getId()));
        }
        filmDTO.setDirectorsIds(directorIds);
    }
}
