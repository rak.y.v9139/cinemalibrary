package com.example.CinemaLibrary.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;

public class OpenAPIConfig {
    @Bean
    public OpenAPI onlineCinema(){
        return new OpenAPI()
                .info(new Info()
                        .title("Онлайн Кинотеатр")
                        .description("Сервис для просмотра кино")
                        .version("v0.1")
                        .license(new License().name("Apache 2.0 "))
                        .contact(new Contact().name("Yaroslav R. Vitalevich")
                                .email("rak@mail.ru")
                                .url(""))
                );
    }
}
