package com.example.CinemaLibrary.constants;

public interface Errors {
    class Users {
        public static final String USER_FORBIDDEN_ERROR = "У вас нет прав просматривать информацию о пользователе";
    }

    class Films {
        public static final String FILM_DELETE_ERROR = "Фильм нельзя удалить, так как у него есть активные аренды.";
    }

    class Directors {
        public static final String DIRECTOR_DELETE_ERROR = "Режиссера нельзя удалить, так как его фильмы находятся в аренде";

    }
}
