package com.example.CinemaLibrary.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String LIBRARIAN = "LIBRARIAN";
    String USER = "USER";
}
