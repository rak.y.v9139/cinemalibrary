package com.example.CinemaLibrary.constants;

public interface MailConstants {
    String MAIL_MESSAGE_FOR_REMEMBER_PASSWORD = """
            Добрый день. Вы получили это электронное письмо, так как от вашего аккаунта был отправлен запрос на восстановление пароля.
                                 Для восстановления пароля перейдите по ссылке: http://localhost:8080/users/change-password?uuid=""";
    
    String MAIL_SUBJECT_FOR_REMEMBER_PASSWORD = "Восстановление пароля на сайте Фильмотеки";
}
