package com.example.CinemaLibrary.model;

public enum Genre {
    ROMCOM("Романтическая комедия"),
    SCIENCE_FICTION("Научная фантастика"),
    HORROR("Ужасы"),
    DOCUMENTARY("Документальный"),
    ANIMATION("Анимационный фильм"),
    ACTION("Боевик"),
    THRILLER("Триллер"),
    DRAMA("Драма"),
    COMEDY("Комедия"),
    ADVENTURE("Приключение");

    private final String genreTextDisplay;

    Genre (String text){
        this.genreTextDisplay = text;
    }

    public String getGenreTextDisplay(){
        return this.genreTextDisplay;
    }
}
