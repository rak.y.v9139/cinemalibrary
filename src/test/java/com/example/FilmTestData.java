package com.example;



import com.example.CinemaLibrary.dto.FilmDTO;
import com.example.CinemaLibrary.model.Film;
import com.example.CinemaLibrary.model.Genre;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public interface FilmTestData {
    FilmDTO FILM_DTO_1 = new FilmDTO(
            "Title1",
            "premierYear1",
            "country",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>(),
            false
    );

    FilmDTO FILM_DTO_2 = new FilmDTO(
            "Title2",
            "premierYear2",
            "country2",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>(),
            false
    );

    List<FilmDTO> FILM_DTO_LIST = Arrays.asList(FILM_DTO_1, FILM_DTO_2);


    Film FILM_1 = new Film(
            "Title2",
            LocalDate.now(),
            "country2",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>()
    );

    Film FILM_2 = new Film(
            "Title3",
            LocalDate.now(),
            "country2",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>()
    );


    List<Film> FILM_LIST = Arrays.asList(FILM_1, FILM_2);

    Set<FilmDTO> FILMS = new HashSet<>(FilmTestData.FILM_DTO_LIST);
}
