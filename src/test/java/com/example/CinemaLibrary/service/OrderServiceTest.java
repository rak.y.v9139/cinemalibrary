package com.example.CinemaLibrary.service;


import com.example.CinemaLibrary.dto.OrderDTO;
import com.example.CinemaLibrary.exception.MyDeleteException;
import com.example.CinemaLibrary.model.Order;
import org.junit.jupiter.api.Test;

public class OrderServiceTest extends GenericTest<Order, OrderDTO> {
    @Test
    @Override
    protected void getAll() {
    
    }
    
    @Test
    @Override
    protected void getOne() {
    
    }
    
    @Test
    @Override
    protected void create() {
    
    }
    
    @Test
    @Override
    protected void update() {
    
    }
    
    @Test
    @Override
    protected void delete() throws MyDeleteException {
    
    }
    
    @Override
    protected void restore() {
    
    }
    
    @Test
    @Override
    protected void getAllNotDeleted() {
    
    }
}
