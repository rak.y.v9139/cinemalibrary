package com.example.CinemaLibrary.service;


import com.example.CinemaLibrary.dto.DirectorDTO;
import com.example.CinemaLibrary.exception.MyDeleteException;
import com.example.CinemaLibrary.mapper.DirectorMapper;
import com.example.CinemaLibrary.model.Director;
import com.example.CinemaLibrary.repository.DirectorRepository;
import com.example.DirectorTestData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;


import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class DirectorServiceTest
        extends GenericTest<Director, DirectorDTO> {

    public DirectorServiceTest() {
        super();
        FilmService filmService = Mockito.mock(FilmService.class);
        repository = Mockito.mock(DirectorRepository.class);
        mapper = Mockito.mock(DirectorMapper.class);
        service = new DirectorService((DirectorRepository) repository, (DirectorMapper) mapper, filmService);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(DirectorTestData.DIRECTOR_LIST);
        Mockito.when(mapper.toDTOs(DirectorTestData.DIRECTOR_LIST)).thenReturn(DirectorTestData.DIRECTOR_DTO_LIST);
        List<DirectorDTO> authorDTOS = service.listAll();
        log.info("Testing getAll(): " + authorDTOS);
        assertEquals(DirectorTestData.DIRECTOR_LIST.size(), authorDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorTestData.DIRECTOR_1));
        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        DirectorDTO directorDTO = service.getOne(1L);
        log.info("Testing getOne(): " + directorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);
    }

    @Order(3)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        DirectorDTO authorDTO = service.create(DirectorTestData.DIRECTOR_DTO_1);
        log.info("Testing create(): " + authorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, authorDTO);
    }

    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        DirectorDTO authorDTO = service.update(DirectorTestData.DIRECTOR_DTO_1);
        log.info("Testing update(): " + authorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, authorDTO);
    }

    @Order(5)
    @Test
    @Override
    protected void delete() throws MyDeleteException {
        Mockito.when(((DirectorRepository) repository).checkDirectorForDeletion(1L)).thenReturn(true);
//        Mockito.when(authorRepository.checkDirectorForDeletion(2L)).thenReturn(false);
        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorTestData.DIRECTOR_1));
        log.info("Testing delete() before: " + DirectorTestData.DIRECTOR_1.isDeleted());
        service.delete(1L);
        log.info("Testing delete() after: " + DirectorTestData.DIRECTOR_1.isDeleted());
        assertTrue(DirectorTestData.DIRECTOR_1.isDeleted());
    }

    @Order(6)
    @Test
    @Override
    protected void restore() {
        DirectorTestData.DIRECTOR_3.setDeleted(true);
        Mockito.when(repository.save(DirectorTestData.DIRECTOR_3)).thenReturn(DirectorTestData.DIRECTOR_3);
        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(DirectorTestData.DIRECTOR_3));
        log.info("Testing restore() before: " + DirectorTestData.DIRECTOR_3.isDeleted());
        ((DirectorService) service).restore(3L);
        log.info("Testing restore() after: " + DirectorTestData.DIRECTOR_3.isDeleted());
        assertFalse(DirectorTestData.DIRECTOR_3.isDeleted());
    }

    @Order(7)
    @Test
    void searchDirectors() {
        PageRequest pageRequest = PageRequest.of(1, 10, Sort.by(Sort.Direction.ASC, "authorFio"));
        Mockito.when(((DirectorRepository) repository).findAllByDirectorsFioContainsIgnoreCaseAndIsDeletedFalse("authorFio1", pageRequest))
                .thenReturn(new PageImpl<>(DirectorTestData.DIRECTOR_LIST));
        Mockito.when(mapper.toDTOs(DirectorTestData.DIRECTOR_LIST)).thenReturn(DirectorTestData.DIRECTOR_DTO_LIST);
        Page<DirectorDTO> authorDTOList = ((DirectorService) service).searchDirectors("authorFio1", pageRequest);
        log.info("Testing searchDirectors(): " + authorDTOList);
        assertEquals(DirectorTestData.DIRECTOR_DTO_LIST, authorDTOList.getContent());
    }



    @Order(8)
    @Test
    protected void getAllNotDeleted() {
        DirectorTestData.DIRECTOR_3.setDeleted(true);
        List<Director> authors = DirectorTestData.DIRECTOR_LIST.stream().filter(Predicate.not(Director::isDeleted)).toList();
        Mockito.when(repository.findAllByIsDeletedFalse()).thenReturn(authors);
        Mockito.when(mapper.toDTOs(authors)).thenReturn(
                DirectorTestData.DIRECTOR_DTO_LIST.stream().filter(Predicate.not(DirectorDTO::isDeleted)).toList());
        List<DirectorDTO> authorDTOS = service.listAllNotDeleted();
        log.info("Testing getAllNotDeleted(): " + authorDTOS);
        assertEquals(authors.size(), authorDTOS.size());
    }

}
