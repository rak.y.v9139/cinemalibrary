package com.example;



import com.example.CinemaLibrary.dto.FilmDTO;
import com.example.CinemaLibrary.dto.OrderDTO;
import com.example.CinemaLibrary.dto.UserDTO;
import com.example.CinemaLibrary.model.Film;
import com.example.CinemaLibrary.model.Order;
import com.example.CinemaLibrary.model.User;

import java.time.LocalDate;
import java.util.List;

public interface OrderTestData {


    OrderDTO FILM_ORDER_DTO = new OrderDTO(
            new UserDTO(),
            new FilmDTO(),
            LocalDate.now(),
            25,
            false,
            1L,
            2L
    );

    List<OrderDTO> FILM_ORDER_DTO_LIST = List.of(FILM_ORDER_DTO);

    Order FILM_ORDER = new Order(
            new User(),
            new Film(),
            LocalDate.now(),
            25,
            false
    );


    List<Order> FILM_ORDER_LIST = List.of(FILM_ORDER);
}
