package com.example;


import com.example.CinemaLibrary.dto.DirectorDTO;
import com.example.CinemaLibrary.model.Director;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;


public interface DirectorTestData {
    DirectorDTO DIRECTOR_DTO_1 = new DirectorDTO("directorFio1",
            "description1",
            new HashSet<>(),
            false);

    DirectorDTO DIRECTOR_DTO_2 = new DirectorDTO("directorFio2",
            "description2",
            new HashSet<>(),
            false);

    DirectorDTO DIRECTOR_DTO_3_DELETED = new DirectorDTO("directorFio3",
            "description3",
            new HashSet<>(),
            true);

    List<DirectorDTO> DIRECTOR_DTO_LIST = Arrays.asList(DIRECTOR_DTO_1, DIRECTOR_DTO_2, DIRECTOR_DTO_3_DELETED);


    Director DIRECTOR_1 = new Director("director1",
            "description1",
            null);

    Director DIRECTOR_2 = new Director("director2",
            "description2",
            null);

    Director DIRECTOR_3 = new Director("director3",
            "description3",
            null);

    List<Director> DIRECTOR_LIST = Arrays.asList(DIRECTOR_1, DIRECTOR_2, DIRECTOR_3);
}

